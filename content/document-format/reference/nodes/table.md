---
title: Node - table
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-12-17"
---

# Node - table

{{% warning %}}
Please note that this is currently only supported on **web** and **desktop**.
**Mobile** rendering support for tables is not available yet.
{{% /warning %}}

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"table"||
|content|✔|array|array of [tableRow](/platform/document-format/reference/nodes/table_row) nodes||
|attrs.isNumberColumnEnabled||object|||
|attrs.layout||string|'default', 'full-width', 'wide'||
