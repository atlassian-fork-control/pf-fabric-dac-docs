---
title: Node - extension
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2018-12-17"
---

# extension
## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"extension"||
|attrs|✔|object|||
|attrs.extensionKey|✔|string|||
|attrs.extensionType|✔|string|||
|attrs.text||string|||
|attrs.parameters||object|||
|attrs.layout||string|"default"|"full-width" or "wide" or "default"|
