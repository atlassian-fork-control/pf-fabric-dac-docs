---
title: Node - applicationCard
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-11-15"
---

# Node - applicationCard

## Purpose

The `applicationCard` represents a rich UI card.

![applicationCard](/platform/document-format/images/applicationCard.png)

## Examples

### Simple application card

```json
{
  "version": 1,
  "type": "doc",
  "content": [
    {
      "type": "applicationCard",
      "attrs": {
        "text": "Donovan Kolbly updated a file: applicationCard.md",
        "title": {
          "text": "Donovan Kolbly updated a file: applicationCard.md"
        }
      }
    }
  ]
}
```

The `text` attribute is not presented in the UI, but is used in the determination of regex matching for [webhooks](https://developer.atlassian.com/cloud/stride/apis/modules/chat/bot-messages/).

### Advanced Application Card

This is the document used to produce the sample rich UI card above.

```json
{
  "version": 1,
  "type": "doc",
  "content": [
    {
      "type": "applicationCard",
      "attrs": {
        "actions": [{
          "key": "unique-app-card-action-key",
          "title": "view dialog",
          "target": {
            "key": "app-dialog"
          }
        }],
        "text": "some text",
        "link": {
          "url": "https://atlassian.com"
        },
        "collapsible": true,
        "title": {
          "text": "Donovan Kolbly updated a file: applicationCard.md",
          "user": {
            "icon": {
              "url": "https://www.gravatar.com/avatar/c3c9338e575a73892b0f1257eb9ee997",
              "label": "Donovan Kolbly"
            }
          }
        },
        "description": {
          "text": "\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis varius mattis massa, quis ornare orci. Integer congue\nrutrum velit, quis euismod eros condimentum quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris\nlobortis nibh id odio egestas luctus. Nunc nulla lacus, congue eu nibh non, imperdiet varius lacus. Nulla sagittis\nmagna et tincidunt volutpat. Nunc augue lorem, eleifend et tempor ut, malesuada ac lorem. Praesent quis feugiat eros,\net vehicula nibh. Maecenas vehicula commodo nisi, at rutrum ipsum posuere sit amet. Integer sit amet nisl sed ligula\nconsectetur feugiat non at ligula. Cras dignissim suscipit magna at mattis. Maecenas ante leo, feugiat vestibulum velit\na, commodo finibus velit. Maecenas interdum ullamcorper velit non suscipit. Proin tempor, magna vitae dapibus laoreet,\nquam dui convallis lectus, in vestibulum arcu eros eu velit. Quisque vel dolor enim.\n"
        },
        "details": [
          {
            "icon": {
              "url": "https://image.ibb.co/fUViW5/board.png",
              "label": "Issue type"
            },
            "text": "Story"
          },
          {
            "badge": {
              "value": 101,
              "max": 99,
              "appearance": "important"
            }
          },
          {
            "lozenge": {
              "text": "Nearly Complete",
              "appearance": "inprogress"
            }
          },
          {
            "title": "Watchers",
            "users": [
              {
                "icon": {
                  "url": "https://www.gravatar.com/avatar/5db869c9686a1d191f99fc153c4d118c.jpg",
                  "label": "Kitty"
                }
              },
              {
                "icon": {
                  "url": "https://www.gravatar.com/avatar/440f0328de63d671bf337779b4eece44.jpg",
                  "label": "Puppy"
                }
              }
            ]
          }
        ],
        "context": {
          "text": "API Documentation / ... / Nodes",
          "icon": {
            "url": "https://image.ibb.co/fPPAB5/API_White_On_Blue.png",
            "label": "api"
          }
        },
        "preview": {
          "url": "https://image.ibb.co/kkFYW5/screenshot_13.png"
        }
      }
    }
  ]
}
```

## Siblings / parents / children

`applicationCard` is a [top-level block](/platform/document-format#top-level-blocks-group-nodes) node and should not be nested with other block-level nodes.

## Marks
`applicationCard` does not support any marks.

## Notes and recommendations

HTTPS is required for image URLs

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"applicationCard"||
|attrs|✔|object|||
|attrs.actions||array|||
|attrs.actions[].target|✔|object|||
|attrs.actions[].target.key|✔|string|||
|attrs.actions[].target.receiver||string|||
|attrs.actions[].title|✔|string|||
|attrs.actions[].key||string||Unique key for action|
|attrs.background||object|||
|attrs.background.url|✔|string|Valid URL pointing to an image with a scheme of https:// or data:image/*||
|attrs.collapsible||boolean||Enables the card to be collapsed by user|
|attrs.context||object|||
|attrs.context.icon||object|||
|attrs.context.icon.label|✔|string|||
|attrs.context.icon.url|✔|string|String matching "^https://\|^data:image/" regular expression||
|attrs.context.text|✔|string|||
|attrs.description||object|||
|attrs.description.text|✔|string||plain text (no HTML allowed)|
|attrs.details||array|||
|attrs.details[].badge||object|||
|attrs.details[].badge.appearance||string|"default" \| "primary" \| "important" \| "added" \| "removed"||
|attrs.details[].badge.max||integer|||
|attrs.details[].badge.theme||string|"default" \| "dark"||
|attrs.details[].badge.value|✔|integer|||
|attrs.details[].icon||object|||
|attrs.details[].icon.url|✔|string|Valid URL pointing to an image with a scheme of https:// or data:image/*||
|attrs.details[].icon.label|✔|string||Label for screen readers|
|attrs.details[].lozenge||object|||
|attrs.details[].lozenge.appearance||string|"default" \| "removed" \| "success" \| "inprogress" \| "new" \| "moved"||
|attrs.details[].lozenge.bold||boolean|||
|attrs.details[].lozenge.text|✔|string|||
|attrs.details[].text||string||plain text (no HTML allowed)|
|attrs.details[].title||string||plain text (no HTML allowed)|
|attrs.details[].users||array|||
|attrs.details[].users[].id||string||Atlassian ID of the user|
|attrs.details[].users[].icon|✔|object|||
|attrs.details[].users[].icon.label|✔|string||Label for screen readers|
|attrs.details[].users[].icon.url|✔|string|Valid URL pointing to an image with a scheme of https:// or data:image/*||
|attrs.link||object|||
|attrs.link.url|✔|string|Valid URL with a scheme of https:// or http://|The click/tap on the card should open the specified URL|
|attrs.preview||object|||
|attrs.preview.url|✔|string|Valid URL pointing to an image with a scheme of https:// or data:image/*||
|attrs.text|✔|string||Fallback for apps whch don't know about this node type|
|attrs.textUrl||string|Valid URL pointing to entity||
|attrs.title|✔|object|||
|attrs.title.text|✔|string||Text in the heading of a card|
|attrs.title.user||object|||
|attrs.title.user.icon|✔|object|||
|attrs.title.user.icon.label|✔|string|||
|attrs.title.user.icon.url|✔|string|String matching "^https://\|^data:image/" regular expression||
|attrs.title.user.id||string|||
