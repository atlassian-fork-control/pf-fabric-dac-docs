---
title: Node - bodiedExtension
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2018-12-17"
---

# bodiedExtension
## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|content|✔|#/definitions/extension_content|||
|type|✔|string|"bodiedExtension"||
|attrs|✔|object|||
|attrs.extensionKey|✔|string|||
|attrs.extensionType|✔|string|||
|attrs.text||string|||
|attrs.parameters||object|||
|attrs.layout||string|"default"|"full-width" or "wide" or "default"|
