---
title: Node - mediaGroup
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-08"
---

# Node - mediaGroup

## Purpose

The `mediaGroup` node is the container for [media](/platform/document-format/reference/nodes/media) nodes.

Without `mediaGroup`, media nodes would be strewn through a document, and grouping would be undefined. The mediaGroup node solves this, by providing an explicit container for media nodes. This means that you can have multiple adjacent groups of media, with unambiguous grouping.

## Example

```json
{
  "type": "mediaGroup",
  "content": [
    {
      "type": "media",
      "attrs": {
        "type": "file",
        "id": "6e7c7f2c-dd7a-499c-bceb-6f32bfbf30b5",
        "collection": "ae730abd-a389-46a7-90eb-c03e75a45bf6",
      }
    },
    {
      "type": "media",
      "attrs": {
        "type": "file",
        "id": "6e7c7f2c-dd7a-499c-bceb-6f32bfbf30b5",
        "collection": "ae730abd-a389-46a7-90eb-c03e75a45bf6",
      }
    }
  ]
}
```

## Content

 * `mediaGroup` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group.
 * `mediaGroup` must only contain nodes of type [media](/platform/document-format/reference/nodes/media).
 * `mediaGroup` must contain 1 or more child nodes (can not be empty)

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"mediaGroup"||
|content|✔|array|array of [media](/platform/document-format/reference/nodes/media) nodes||
