---
title: Node - inlineExtension
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2017-12-29"
---

# inlineExtension
## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"inlineExtension"||
|attrs|✔|object|||
|attrs.extensionKey|✔|string|||
|attrs.text||string|||
|attrs.extensionType|✔|string|||
