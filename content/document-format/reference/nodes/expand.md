---
title: Node - expand
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2019-12-17"
---

# Node - expand

## Purpose

The `expand` node is a container that allows content to be hidden or shown, similar to an accordion or disclosure widget.

This node cannot be nested within [TableCell](/platform/document-format/reference/nodes/table_cell) or [TableHeader](/platform/document-format/reference/nodes/table_header), you may add a [NestedExpand](/platform/document-format/reference/nodes/nestedExpand) instead

## Example

```json
{
  "type": "expand",
  "attrs": {
    "title": "Hello world"
  },
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello world"
        }
      ]
    }
  ]
}
```

## Content

`expand` is a block node in the [top-level blocks](/platform/document-format#-top-level-blocks--group-nodes) group that can contain an array of one-or-more:

- [Paragraph](/platform/document-format/reference/nodes/paragraph)
- [Heading](/platform/document-format/reference/nodes/heading)
- [OrderedList](/platform/document-format/reference/nodes/orderedList)
- [BulletList](/platform/document-format/reference/nodes/bulletList)
- [Table](/platform/document-format/reference/nodes/table)
- [Extension](/platform/document-format/reference/nodes/extension)
- [Panel](/platform/document-format/reference/nodes/panel)
- [Blockquote](/platform/document-format/reference/nodes/blockquote)
- [Rule](/platform/document-format/reference/nodes/rule)
- [Codeblock](/platform/document-format/reference/nodes/codeBlock)
- [MediaGroup](/platform/document-format/reference/nodes/mediaGroup)
- [MediaSingle](/platform/document-format/reference/nodes/mediaSingle)
- [DecisionList](/platform/document-format/reference/nodes/decisionList)
- [TaskList](/platform/document-format/reference/nodes/taskList)


## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|content|✔|Array of one-or-more above mentioned nodes.|||
|type|✔|string|"expand"||
|attrs|✔|object|||
|attrs.title||string|||
|marks||array|An optional "breakout" mark||
