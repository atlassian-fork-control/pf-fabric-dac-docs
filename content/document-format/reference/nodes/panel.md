---
title: Node - panel
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-11-21"
---

# Node - panel

## Purpose

The `panel` node is a container that allows content to be promoted in a document.

## Example

```json
{
  "type": "panel",
  "attrs": {
    "panelType": "info"
  },
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello world"
        }
      ]
    }
  ]
}
```

## Content

`panel` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that can contain array of one-or-more:

- [Paragraph](/platform/document-format/reference/nodes/paragraph)
- [Heading](/platform/document-format/reference/nodes/heading)
- [OrderedList](/platform/document-format/reference/nodes/orderedList)
- [BulletList](/platform/document-format/reference/nodes/bulletList)

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"panel"||
|content|✔|array|Array of one-or-more above mentioned nodes.|Panels must have at least one child|
|attrs|✔|object|||
|attrs.panelType|✔|string|"info", "note", "warning", "success", "error"||
