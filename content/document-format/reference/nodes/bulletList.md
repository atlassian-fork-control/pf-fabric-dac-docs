---
title: Node - bulletList
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-08"
---
# Node - bulletList

## Purpose

The `bulletList` node is a container for [listItem](/platform/document-format/reference/nodes/listItem) nodes.

## Example

```json
{
  "type": "bulletList",
  "content": [
    {
      "type": "listItem",
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "type": "text",
              "text": "Hello world"
            }
          ]
        }
      ]
    }
  ]
}
```

## Content

`bulletList` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that contains [listItem](/platform/document-format/reference/nodes/listItem) nodes as children.

## Fields

|Name   |Mandatory|Type  |Value      |Notes|
|-------|---------|------|-----------|-----|
|type|✔|string|"bulletList"||
|content|✔|array|Array of [listItem](/platform/document-format/reference/nodes/listItem) nodes||
