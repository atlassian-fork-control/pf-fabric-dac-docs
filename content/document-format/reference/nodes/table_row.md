---
title: Node - tableRow
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-13"
---

# Node - tableRow

{{% warning %}}
Please note that this is currently only supported on web and desktop.
Mobile rendering support for tables is not available yet.
{{% /warning %}}

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"tableRow"||
|content|✔|array|array of [tableHeader](/platform/document-format/reference/nodes/table_header) or [tableCell](/platform/document-format/reference/nodes/table_cell) nodes||
