---
title: Node - rule
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-08"
---
# Node - rule

## Purpose

The `rule` node is an block node that represents a divider (e.g. &lt;hr/&gt;).

## Example

```json
{
  "version": 1,
  "type": "doc",
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello world"
        }
      ]
    },
    {
      "type": "rule"
    }
  ]
}
```

## Content

`rule` is an immutable block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that can not be modified.

## Marks

`rule` does not support any marks.

## Fields

|Name   |Mandatory|Type  |Value      |Notes|
|-------|---------|------|-----------|-----|
|type|✔|string|"rule"||
