---
title: Node - doc
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-12-17"
---
# Node - doc

## Purpose

The `doc` node is the root container node representing a document.

## Example

```json
{
  "version": 1,
  "type": "doc",
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello world"
        }
      ]
    }
  ]
}
```

## Content

`doc` node contains [top-level block](/platform/document-format#top-level-blocks-group-nodes) nodes, or a layoutSection.

## Fields

|Name   |Mandatory|Type  |Value      |Notes|
|-------|---------|------|-----------|-----|
|type|✔|string|"doc"||
|version|✔|integer|1||
|content|✔|array|Array of zero or more **top-level blocks** nodes.|Can support any **top-level blocks** node children|
