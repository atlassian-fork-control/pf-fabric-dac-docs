---
title: Node - decisionList
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2017-10-27"
---

# Node - decisionList

## Purpose

The `decisionList` node is a container for [decisionItem](/platform/document-format/reference/nodes/decisionItem) nodes.

## Example

```json
{
  "type": "decisionList",
  "attrs": {
    "localId": "decision-list-id"
  },
  "content": [
    {
      "type": "decisionItem",
      "attrs": {
        "localId": "item-test-id",
        "state": "DECIDED"
      },
      "content": [
        {
          "type": "text",
          "text": "Start using Document Format for apps"
        }
      ]
    }
  ]
}
```

## Content

`decisionList` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that contains [decisionItem](/platform/document-format/reference/nodes/decisionItem) nodes as children.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"decisionList"||
|content|✔|array|Array of [decisionItem](/platform/document-format/reference/nodes/decisionItem) nodes||
|attrs|✔|object|||
|attrs.localId|✔|string|UUID|This should be a unique identifier per document|
