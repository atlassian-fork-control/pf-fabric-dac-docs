---
title: Node - emoji
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-08"
---

# Node - emoji

## Purpose

The `emoji` node is an inline node that represents an emoji.

There are three kinds of emoji:

 * Standard — Unicode emoji
 * Atlassian — Non-standard emoji introduced by Atlassian
 * Site — Non-standard customer defined emoji

## Examples

### Unicode emoji

```json
{
  "type": "emoji",
  "attrs": {
    "shortName": ":grinning:",
    "text": "😀"
  }
}
```

### Non-standard Atlassian emoji

```json
{
  "type": "emoji",
  "attrs": {
    "shortName": ":awthanks:",
    "id": "atlassian-awthanks",
    "text": ":awthanks:"
  }
}
```

### Non-standard customer emoji

```json
{
  "type": "emoji",
  "attrs": {
    "shortName": ":thumbsup::skin-tone-2:",
    "id": "1f44d",
    "text": "👍🏽"
  }
}
```

## Content

`emoji` is an immutable node in the [inlines](/platform/document-format#inlines-group-nodes) group that can not be modified.

## Marks

`emoji` does not support any marks.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"emoji"||
|attrs|✔|object|||
|attrs.id||string|Emoji service ID of the emoji. e.g. "1f600"|The value varies based on the kind, but it is not intended to have any user facing meaning (standard — 1f3f3-1f308, Atlassian — atlassian-&lt;shortName&gt;, Site — 13d29267-ff9e-4892-a484-1a1eef3b5ca3)|
|attrs.shortName|✔|string|Minimum information to store and render an emoji||
|attrs.text||string|The shortName will be rendered in place of this if this is omitted||
