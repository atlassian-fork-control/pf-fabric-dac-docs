---
title: Node - mention
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2017-11-22"
---

# Node - mention

## Purpose

The `mention` node is an **inline** node that represents a user mention.

## Examples

```json
{
  "type": "mention",
  "attrs": {
    "id": "ABCDE-ABCDE-ABCDE-ABCDE",
    "text": "@Bradley Ayers",
    "userType": "APP"
  }
}
```

## Content

`mention` is an immutable node in the [inlines](/platform/document-format#inlines-group-nodes) group that can not be modified.

## Marks

`mention` does not support any marks.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"mention"||
|attrs|✔|object|||
|attrs.accessLevel||string|"NONE" \| "SITE" \| "APPLICATION" \| "CONTAINER"||
|attrs.id|✔|string|Atlassian Account ID or collection name|A mention can reference two different kinds of entities: single user (e.g. @BradleyAyers) or an Atlassian Account ID (i.e. ABCDE-ABCDE-ABCDE-ABCDE|
|attrs.text||string||The textual representation of the mention, including a leading @|
|attrs.userType||string|"DEFAULT" \| "SPECIAL" \| "APP"||
