---
title: Node - mediaSingle
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2019-04-17"
---

# Node - mediaSingle

## Purpose

The `mediaSingle` node is the container for *one* [media](/platform/document-format/reference/nodes/media) node.

The intent is that it displays its content in full, in contrast to a [mediaGroup](/platform/document-format/reference/nodes/mediaGroup), which is intended for a list of attachments. A common use case is to display an image, but it can also be used for videos, or other types of content usually renderable by a `@atlaskit/media` Card component.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|content|✔|#/definitions/media_node|||
|type|✔|string|"mediaSingle"||
|attrs|✔|object|||
|attrs.layout|✔|string|"wrap-left" \| "center" \| "wrap-right" \| "wide" \| "full-width"\| "align-start" \| "align-end" |`wrap-left` and `wrap-right` provide an image floated to the left or right of the page respectively, with text wrapped around it.<br /><br />`center` center aligns the image as a block, while `wide` does the same but bleeds into the margins. `full-width` makes the image stretch from edge to edge of the page.|
|attrs.width||number|floating point number between 0 and 100|Determines the width of the image as a percentage of the text line-length (width of the textual content area).<br /><br />Has no effect if layout mode is `wide` or `full-width`.|