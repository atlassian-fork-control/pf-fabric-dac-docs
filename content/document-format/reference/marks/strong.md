---
title: Mark - strong
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-08"
---
# Mark - strong

## Purpose

The `strong` mark is intended to be applied to [text](/platform/document-format/reference/nodes/text) nodes to indicate **strong** styling.

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "strong"
    }
  ]
}
```

## Fields

|Name|Mandatory|Type  |Value   |Notes|
|----|---------|------|--------|-----|
|type|✔        |string|"strong"|     |
