---
title: Mark - indentation
platform: none
product: document-format
category: reference
subcategory: marks
date: "2019-07-01"
---

# Mark - indentation

## Purpose

The `indentation` mark is intended to be applied to paragraph and headings to indent these nodes to a particular level (upto 6).

## Example

```json
 {
    "type": "paragraph",
    "content": [
      {
        "type": "text",
        "text": "hello world"
      }
    ],
    "marks": [
      {
        "type": "indentation",
        "attrs": {
          "level": 4
        }
      }
    ]
  }
```

## Fields

| Name       | Mandatory | Type   | Value                  |
| ---------- | --------- | ------ | ---------------------- |
| type       | ✔         | string | "indentation"             |
| attrs      | ✔         | object |                        |
| attrs.level | ✔         | number | 1 - 6 |
