---
title: Mark - strike
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-08"
---
# Mark - strike

## Purpose

The `strike` mark is intended to be applied to [text](/platform/document-format/reference/nodes/text) nodes to indicate <s>strike-through</s> styling.

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "strike"
    }
  ]
}
```

## Fields

|Name|Mandatory|Type  |Value   |Notes|
|----|---------|------|--------|-----|
|type|✔        |string|"strike"|     |
