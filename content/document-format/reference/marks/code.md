---
title: Mark - code
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-08"
---
# Mark - code

## Purpose

The `code` mark is intended to be applied to [text](/platform/document-format/reference/nodes/text) nodes to indicate code styling. Code styling is a distinct concept from monospace (mono) because:

 * user mentioning is not possible inside code
 * inserting emoji is not possible inside code
 * code does not allow other marks to be interspersed (e.g. bold / italic / links within code)
 * code is rendered with background decoration

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "code"
    }
  ]
}
```

## Fields

|Name|Mandatory|Type  |Value |Notes|
|----|---------|------|------|-----|
|type|✔        |string|"code"|     |
