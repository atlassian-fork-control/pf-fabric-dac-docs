---
title: Mark - underline
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-08"
---
# Mark - underline

## Purpose

The `underline` mark is intended to be applied to [text](/platform/document-format/reference/nodes/text) nodes to indicate __underline__ styling.

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "underline"
    }
  ]
}
```

## Fields

|Name|Mandatory|Type  |Value   |Notes|
|----|---------|------|--------|-----|
|type|✔        |string|"underline"|     |
