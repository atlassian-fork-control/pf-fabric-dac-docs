---
title: Mark - subsup
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-08"
---

# Mark - subsup

## Purpose

The `subsup` mark is intended to be applied to [text](/platform/document-format/reference/nodes/text) nodes to indicate <sup>superscript</sup> or <sub>subscript</sub> styling.

These two types of styling are mutually exclusive, so a single mark is used to represent both.

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "subsup",
      "attrs": {
        "type": "sub"
      }
    }
  ]
}
```

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"subsup"||
|attrs|✔|object|||
|attrs.type|✔|string|"sub" \| "sup"|Indicates whether superscript or subscript styling applies|
