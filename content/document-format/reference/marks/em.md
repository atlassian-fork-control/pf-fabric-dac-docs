---
title: Mark - em
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-08"
---
# Mark - em

## Purpose

The `em` mark is intended to be applied to [text](/platform/document-format/reference/nodes/text) nodes to indicate *italic* styling.

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "em"
    }
  ]
}
```

## Fields

|Name|Mandatory|Type  |Value |Notes|
|----|---------|------|------|-----|
|type|✔        |string|"em"  |     |
