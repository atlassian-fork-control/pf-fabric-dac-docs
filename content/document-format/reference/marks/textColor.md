---
title: Mark - textColor
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-08"
---

# Mark - textColor

## Purpose

The `textColor` mark is intended to be applied to text nodes to indicate <style="color: red">colour</style> styling.

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "textColor",
      "attrs": {
        "color": "#97a0af"
      }
    }
  ]
}
```

## Combination with other marks

`textColor` can **NOT** be combined with the following marks:

- [code](/platform/document-format/reference/marks/code)
- [link](/platform/document-format/reference/marks/link)

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"textColor"||
|attrs|✔|object|||
|attrs.color|✔|string|String matching "^#[0-9a-f]{6}$" regular expression||
