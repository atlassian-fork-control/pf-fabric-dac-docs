---
title: Mark - annotation
platform: none
product: document-format
category: reference
subcategory: marks
date: "2019-07-02"
---

# Mark - annotation

## Purpose

The `annotation` mark is intended to be applied to a paragraph to annotate or label the paragraph or a part of it. 

## Example

```json
 {
  "type": "paragraph",
  "content": [
    {
      "type": "text",
      "text": "hello "
    },
    {
      "type": "text",
      "text": "there",
      "marks": [
        {
          "type": "annotation",
          "attrs": {
            "id": "123456",
            "annotationType": "inlineComment"
          }
        }
      ]
    }
  ]
}
```

## Fields

| Name       | Mandatory | Type   | Value                  |
| ---------- | --------- | ------ | ---------------------- |
| type       | ✔         | string | "annotation"           |
| attrs      | ✔         | object |                        |
| attrs.id | ✔         | string | "12334", etc             |
| attrs.annotationType | ✔         | string | "inlineComment", etc |
