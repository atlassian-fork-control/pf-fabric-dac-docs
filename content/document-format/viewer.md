---
title: Document builder
platform: platform
product: document-format
category: devguide
subcategory: misc
date: "2018-04-19"
---
# Document viewer

Insert an Atlassian Document Format (ADF) message in the input area and see its UI representation below.

*(If nothing shows below, just wait a few seconds for the viewer to load)*

<iframe src="https://ak-mk-2-prod.netlify.com/examples.html?groupId=editor&packageId=renderer&exampleId=dac-viewer" style="border: none; width: 100%; height: 640px"></iframe>
